#include <stm32f0xx.h>
#include "usb_lib\usb_lib.h"
#include <string.h>
#include <stdlib.h>
#include "SEGGER_RTT.h"

#define S1_H        GPIOB -> BSRR = GPIO_BSRR_BS_11
#define S1_L        GPIOB -> BSRR = GPIO_BSRR_BR_11
#define S2_H        GPIOB -> BSRR = GPIO_BSRR_BS_10
#define S2_L        GPIOB -> BSRR = GPIO_BSRR_BR_10

#define LED_ON      GPIOB -> BSRR = GPIO_BSRR_BR_12
#define LED_OFF     GPIOB -> BSRR = GPIO_BSRR_BS_12

#define N_CHANNEL   40
#define SETTINGS_BYTES_N    10
#define LED_PAUSE   1000

typedef struct {
    uint16_t volume;
    uint16_t pitch;
    uint16_t level_l;
    uint16_t level_h;
    uint16_t pause;
} _settings;

typedef struct {
    uint8_t state;
    uint16_t ampl;
    uint16_t time;
    uint16_t pause_cnt;
} _channel; 

typedef struct {
    uint8_t state;
    uint8_t volume;
} _midi_cmd;

_channel channel[N_CHANNEL];
_settings settings[N_CHANNEL];
_midi_cmd midi_cmd[N_CHANNEL];
uint16_t adc_buf[N_CHANNEL];
uint8_t midi_channel;

uint8_t pinout[40] = {
    7, 2, 39, 34, 31, 26, 22, 19, 15, 10,
    4, 1, 36, 33, 28, 25, 21, 16, 12, 9,
    6, 0, 38, 32, 30, 24, 20, 18, 14, 8,
    5, 3, 37, 35, 29, 27, 23, 17, 13, 11
};

void SettingsToByteArrayN10(_settings s, uint8_t *bytes){
    bytes[0] = (s.volume >> 8) & 0xFF;
    bytes[1] = s.volume & 0xFF;
    bytes[2] = (s.pitch >> 8) & 0xFF;
    bytes[3] = s.pitch & 0xFF;
    bytes[4] = (s.level_l >> 8) & 0xFF;
    bytes[5] = s.level_l & 0xFF;
    bytes[6] = (s.level_h >> 8) & 0xFF;
    bytes[7] = s.level_h & 0xFF;
    bytes[8] = (s.pause >> 8) & 0xFF;
    bytes[9] = s.pause & 0xFF;
}

void ByteArrayN10ToSettings(uint8_t *bytes, _settings *s){
    s -> volume = (bytes[0] << 8) | bytes[1];
    s -> pitch = (bytes[2] << 8) | bytes[3];
    s -> level_l = (bytes[4] << 8) | bytes[5];
    s -> level_h = (bytes[6] << 8) | bytes[7];
    s -> pause = (bytes[8] << 8) | bytes[9];
}

void SettingsToHWordsArrayN5(_settings s, uint16_t *hword){
    hword[0] = s.volume;
    hword[1] = s.pitch;
    hword[2] = s.level_l;
    hword[3] = s.level_h;
    hword[4] = s.pause;
}

void HWordsArrayN5ToSettings(uint16_t *hword, _settings *s){
    s -> volume = hword[0];
    s -> pitch = hword[1];
    s -> level_l = hword[2];
    s -> level_h = hword[3];
    s -> pause = hword[4];
}

void SetClockHSI48(){
    RCC -> APB1ENR |= RCC_APB1ENR_CRSEN;
    RCC -> CR2 |= RCC_CR2_HSI48ON;
    while (!(RCC -> CR2 & RCC_CR2_HSI48RDY));
    RCC -> CFGR3 &= ~RCC_CFGR3_USBSW;
    FLASH -> ACR = FLASH_ACR_PRFTBE | FLASH_ACR_LATENCY;
    CRS -> CR |= CRS_CR_AUTOTRIMEN;
    CRS -> CR |= CRS_CR_CEN;
    RCC -> CFGR |= RCC_CFGR_SW;
}

void Read_Settings(){
    uint8_t i, j;
    uint16_t hwords[SETTINGS_BYTES_N / 2];

    for (i = 0; i < N_CHANNEL; i++){
        for (j = 0; j < SETTINGS_BYTES_N/2; j++){
            hwords[j] = *(uint16_t*)(0x08003C00 + i*SETTINGS_BYTES_N + j*2);
        }
        HWordsArrayN5ToSettings(hwords, &settings[i]);
        if (settings[i].volume == 0xFFFF) settings[i].volume = 2000;
        if (settings[i].pitch == 0xFFFF) settings[i].pitch = i;
        if (settings[i].level_l == 0xFFFF) settings[i].level_l = 10;
        if (settings[i].level_h == 0xFFFF) settings[i].level_h = 500;
        if (settings[i].pause == 0xFFFF) settings[i].pause = 1;
    }
    midi_channel = (uint8_t)*(uint16_t*)(0x08003C00 + (N_CHANNEL + 1)*SETTINGS_BYTES_N);
}

uint8_t Save_Settings(){
    uint8_t i, j;
    uint16_t hwords[SETTINGS_BYTES_N / 2];
    if (FLASH -> CR & FLASH_CR_LOCK){
	FLASH -> KEYR = 0x45670123;
	FLASH -> KEYR = 0xCDEF89AB;
    }
    //ERASE
    FLASH -> CR |= FLASH_CR_PER;
    FLASH -> AR = 0x08003C00;
    FLASH -> CR |= FLASH_CR_STRT;
    while(FLASH -> SR & FLASH_SR_BSY);
    if (FLASH -> SR & FLASH_SR_PGERR) return FLASH -> SR;
    FLASH -> CR &= ~FLASH_CR_PER;

    FLASH -> CR |= FLASH_CR_PG;
    for (i = 0; i < N_CHANNEL; i++){
        SettingsToHWordsArrayN5(settings[i], hwords);
        for (j = 0; j < SETTINGS_BYTES_N/2; j++){
            *(uint16_t*)(0x08003C00 + i*SETTINGS_BYTES_N + j*2) = hwords[j];
            while(FLASH -> SR & FLASH_SR_BSY);
        }
    }
    *(uint16_t*)(0x08003C00 + (N_CHANNEL + 1)*SETTINGS_BYTES_N) = (uint16_t)midi_channel;
    FLASH -> CR &= ~FLASH_CR_PG;
    if (FLASH -> SR & FLASH_SR_PGERR) return FLASH -> SR;
    return 0;
}

void EP_Handler(uint8_t ep_num){
    uint8_t buf[20], txt[10], i, send_buf[SETTINGS_BYTES_N + 2];
    if (ep_num == 2){
        if (endpoints[ep_num].rx_flag){
            EP_Read(ep_num, buf);
            SEGGER_RTT_WriteString(0, "RX ");
            for (i = 0; i < endpoints[ep_num].rx_cnt; i++){
                itoa(buf[i], txt, 16);
                SEGGER_RTT_WriteString(0, txt);
                SEGGER_RTT_WriteString(0, " ");
            }
            SEGGER_RTT_WriteString(0, "\r\n");
            if ((buf[0] == 1) && (endpoints[ep_num].rx_cnt == 12)){
                ByteArrayN10ToSettings(&buf[2], &settings[buf[1]]);
            }
            if ((buf[0] == 2) && (buf[1] == 0)){
                SEGGER_RTT_WriteString(0, "TX ");
                send_buf[0] = 3;
                itoa(send_buf[0], txt, 16);
                SEGGER_RTT_WriteString(0, txt);
                SEGGER_RTT_WriteString(0, " ");
                send_buf[1] = buf[2];
                itoa(send_buf[1], txt, 16);
                SEGGER_RTT_WriteString(0, txt);
                SEGGER_RTT_WriteString(0, " ");
                if (buf[2] < N_CHANNEL){
                    SettingsToByteArrayN10(settings[buf[2]], &send_buf[2]);
                    for (i = 2; i < SETTINGS_BYTES_N + 2; i++){
                        itoa(send_buf[i], txt, 16);
                        SEGGER_RTT_WriteString(0, txt);
                        SEGGER_RTT_WriteString(0, " ");
                    }
                } else {
                    send_buf[2] = midi_channel;
                    itoa(send_buf[2], txt, 16);
                    SEGGER_RTT_WriteString(0, txt);
                    SEGGER_RTT_WriteString(0, " ");
                    for (i = 3; i < SETTINGS_BYTES_N + 2; i++){
                        send_buf[i] = 0;
                        itoa(send_buf[i], txt, 16);
                        SEGGER_RTT_WriteString(0, txt);
                        SEGGER_RTT_WriteString(0, " ");
                    }
                }
                SEGGER_RTT_WriteString(0, "\r\n");
                EP_Write_NoBlock(2, send_buf, SETTINGS_BYTES_N+2);
            } else 
            if ((buf[0] == 2) && (buf[1] == 1)){
                Save_Settings();
                SEGGER_RTT_WriteString(0, "SAVE OK\r\n");
            } else 
            if ((buf[0] == 2) && (buf[1] == 2)){
                midi_channel = buf[2];
                SEGGER_RTT_WriteString(0, "MIDI Channel changed\r\n");
            } 
            endpoints[ep_num].status = SET_VALID_RX(endpoints[ep_num].status);
            endpoints[ep_num].status = KEEP_STAT_TX(endpoints[ep_num].status);    
        }
        if (endpoints[ep_num].tx_flag){
            endpoints[ep_num].tx_flag = 0;
            SEGGER_RTT_WriteString(0, "TX OK\r\n");
        }
    }
}

void EP_UserInit(){
    EP_Init(1, EP_TYPE_BULK, 384, 512);
    EP_Init(2, EP_TYPE_INTERRUPT, 600, 700);
}

void ADC_COMP_IRQHandler(){
    static uint8_t state = 0;
    uint16_t i;
    if (ADC1 -> ISR & ADC_ISR_EOSEQ){
        ADC1 -> ISR = ADC_ISR_EOSEQ;
        if (state < 3) state++; else state = 0;

        switch (state){
        case 0:
            S2_L;
            S1_L;
            break;
        case 1:
            S2_L;
            S1_H;
            break;
        case 2:
            S1_L;
            S2_H;
            break;
        case 3:
            S2_H;
            S1_H;
            break;
        default:
            state = 0;
            break;
        }
        ADC1 -> CR |= ADC_CR_ADSTART;
    }
}

void DMA1_CH1_IRQHandler(){
    uint8_t i, j;
    if (DMA1 -> ISR & DMA_ISR_TCIF1){
        DMA1 -> IFCR = DMA_IFCR_CTCIF1;

        for (i = 0; i < N_CHANNEL; i++){
            j = pinout[i];
            if (channel[j].state == 0){
                if (adc_buf[i] >= settings[j].level_h){
                    channel[j].state = 1;
                    channel[j].ampl = adc_buf[i];
                    channel[j].time = 0;
                    channel[j].pause_cnt = settings[j].pause;
                }
            } else {
                if (channel[j].state == 2){
                    channel[j].time++;
                    if (adc_buf[i] <= settings[j].level_l){
                        channel[j].state = 3;
                    }
                } else if (channel[j].state == 3){
                    if (channel[j].pause_cnt) channel[j].pause_cnt--;
                    else {
                        channel[j].state = 0;
                    }
                }
            }
        }
    }
}

void ADC_Init(){
    RCC -> AHBENR |= RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN | RCC_AHBENR_DMA1EN;
    RCC -> APB2ENR |= RCC_APB2ENR_ADC1EN;

    GPIOA -> OSPEEDR = 0xFFFFFFFF;
    GPIOB -> OSPEEDR = 0xFFFFFFFF;
    GPIOB -> MODER |= GPIO_MODER_MODER10_0 | GPIO_MODER_MODER11_0;
    S1_L;
    S2_L;
    GPIOA -> MODER |= 0x0000FFFF;
    GPIOB -> MODER |= GPIO_MODER_MODER0 | GPIO_MODER_MODER1;

    ADC1 -> CFGR2 = ADC_CFGR2_JITOFFDIV4;
    ADC1 -> CR = ADC_CR_ADCAL;
    while (ADC1 -> CR & ADC_CR_ADCAL);
    ADC1 -> CR = ADC_CR_ADEN;
    ADC1 -> CR = ADC_CR_ADEN;
    while (!(ADC1 -> ISR & ADC_ISR_ADRDY));
    
    ADC1 -> CFGR1 = ADC_CFGR1_DMACFG | ADC_CFGR1_DMAEN;
    ADC1 -> SMPR = ADC_SMPR_SMP;
    ADC1 -> CHSELR = 0x03FF;
    ADC1 -> IER = ADC_IER_EOSEQIE;

    DMA1_Channel1 -> CMAR = (uint32_t)adc_buf;
    DMA1_Channel1 -> CPAR = (uint32_t)&ADC1 -> DR;
    DMA1_Channel1 -> CNDTR = N_CHANNEL;
    DMA1_Channel1 -> CCR = DMA_CCR_PL | DMA_CCR_MSIZE_0 | DMA_CCR_PSIZE_0 | DMA_CCR_MINC | DMA_CCR_CIRC | 
	DMA_CCR_TCIE | DMA_CCR_EN;

    ADC1 -> CR |= ADC_CR_ADSTART;
    NVIC_EnableIRQ(DMA1_Ch1_IRQn);
    NVIC_EnableIRQ(ADC1_COMP_IRQn);
} 



void LED_Init(){
    RCC -> AHBENR |= RCC_AHBENR_GPIOBEN;
    GPIOB -> MODER |= GPIO_MODER_MODER12_0;
    GPIOB -> OTYPER |= GPIO_OTYPER_OT_12;
    GPIOB -> OSPEEDR |= GPIO_OSPEEDER_OSPEEDR12;
    LED_OFF;
}

void main(void){
    uint8_t i, str[10], tx_buf[6], j;
    uint16_t led_counter = 0;
    SetClockHSI48();
    USB_Init();
    Read_Settings();
    SEGGER_RTT_Init();
    ADC_Init();
    LED_Init();

    IWDG -> KR = 0x0000CCCC;
    IWDG -> KR = 0x00005555;
    IWDG -> PR = 7;
    IWDG -> RLR = 0xFF;
    while (IWDG -> SR);
    IWDG -> KR = 0x0000AAAA;

    while (1){
        IWDG -> KR = 0x0000AAAA;
        Enumerate(0);

        if (led_counter < LED_PAUSE) led_counter++;
        else {
            LED_OFF;
        }

        if (USB_GetState() == USB_HID_READY){
            for (i = 0; i < N_CHANNEL; i++){
                if (channel[i].state == 1){
                    SEGGER_RTT_WriteString(0, "Ch ");
                    itoa(i+1, str, 10);
                    SEGGER_RTT_WriteString(0, str);
                    SEGGER_RTT_WriteString(0, " Ampl ");
                    itoa(channel[i].ampl, str, 10);
                    SEGGER_RTT_WriteString(0, str);
                    SEGGER_RTT_WriteString(0, " Time ");
                    itoa(channel[i].time, str, 10);
                    SEGGER_RTT_WriteString(0, str);
                    SEGGER_RTT_WriteString(0, "\r\n");
                    channel[i].state = 2;

                    midi_cmd[i].state = 1;

                    LED_ON;
                    led_counter = 0;
                    
                    if (channel[i].ampl > settings[i].volume) channel[i].ampl = settings[i].volume;
                    midi_cmd[i].volume = (0x7f * channel[i].ampl) / settings[i].volume;

                    tx_buf[0] = 0x09;
                    tx_buf[1] = 0x90 | (midi_channel & 0x0F);
                    tx_buf[2] = settings[i].pitch;
                    tx_buf[3] = midi_cmd[i].volume;

                    SEGGER_RTT_WriteString(0, "MIDI ");
                    for (j = 0; j < 4; j++){
                        itoa(tx_buf[j], str, 16);
                        SEGGER_RTT_WriteString(0, str);
                        SEGGER_RTT_WriteString(0, " ");
                    }
                    SEGGER_RTT_WriteString(0, "\r\n");

                    EP_Write(1, tx_buf, 4);
                }
            }
            for (i = 0; i < N_CHANNEL; i++){
                if (midi_cmd[i].state){
                    tx_buf[0] = 0x08;
                    tx_buf[1] = 0x80 | (midi_channel & 0x0F);
                    tx_buf[2] = settings[i].pitch;
                    tx_buf[3] = 0;
                    midi_cmd[i].state = 0;
                    EP_Write(1, tx_buf, 4);
                }
            }
        }
        EP_Handler(2);
    }
}

