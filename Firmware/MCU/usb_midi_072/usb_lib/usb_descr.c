#include "usb_descr.h"

const uint8_t USB_DeviceDescriptor[] = {
    0x12,	//bLength
    0x01,	//bDescriptorType
    0x10,	//bcdUSB_L
    0x01,	//bcdUSB_H
    0x00,	//bDeviceClass
    0x00,	//bDeviceSubClass
    0x00,	//bDeviceProtocol
    0x40,	//bMaxPacketSize
    0x83,	//idVendor_L
    0x04,	//idVendor_H
    0x11,	//idProduct_L
    0x57,	//idProduct_H
    0x01,	//bcdDevice_Ver_L
    0x00,	//bcdDevice_Ver_H
    0x01,	//iManufacturer
    0x02,	//iProduct
    0x03,	//iSerialNumber
    0x01	//bNumConfigurations
};

const uint8_t USB_DeviceQualifierDescriptor[] = {
    0x0A,	//bLength
    0x06,	//bDescriptorType
    0x00,	//bcdUSB_L
    0x02,	//bcdUSB_H
    0x00,	//bDeviceClass
    0x00,	//bDeviceSubClass
    0x00,	//bDeviceProtocol
    0x40,	//bMaxPacketSize0
    0x01,	//bNumConfigurations
    0x00	//Reserved
};

const uint8_t USB_ConfigDescriptor[] = {
//���������� ������������
    0x09,	//bLength
    0x02,	//bDescriptorType
    CONFIG_DESCRIPTOR_SIZE_BYTE,	//wTotalLength_L
    0x00,	//wTotalLength_H
    0x03,	//bNumInterfaces
    0x01,	//bConfigurationValue
    0x00,	//iConfiguration
    0x80,	//bmAttributes
    0x32,	//bMaxPower
//���������� ����������
    0x09,	//bLength
    0x04,	//bDescriptorType
    0x00,	//bInterfaceNumber
    0x00,	//bAlternateSetting
    0x00,	//bNumEndpoints
    0x01,	//bInterfaceClass(0x01 - Audio, 0x03 - HID, 0xFF - custom)
    0x01,	//bInterfaceSubClass
    0x00,	//bInterfaceProtocol
    0x00,	//iInterface
    //���������� AC ����������
    0x09,	//bLength
    0x24,	//bDescriptorType
    0x01,	//bDescriptorSubType
    0x00,	//bcdADC_L
    0x01,	//bcdADC_H
    0x09,	//wTotalLength_L
    0x00,	//wTotalLength_H
    0x01,	//blnCollection
    0x01,	//baInterfaceNr(1)
//���������� ����������
    0x09,	//bLength
    0x04,	//bDescriptorType
    0x01,	//bInterfaceNumber
    0x00,	//bAlternateSetting
    0x01,	//bNumEndpoints
    0x01,	//bInterfaceClass(0x01 - Audio, 0x03 - HID, 0xFF - custom)
    0x03,	//bInterfaceSubClass
    0x00,	//bInterfaceProtocol
    0x00,	//iInterface
    //���������� MS ����������
    0x07,	//bLength
    0x24,	//bDescriptorType
    0x01,	//bDescriptorSubType
    0x00,	//bcdADC_L
    0x01,	//bcdADC_H
    0x20, 	//wTotalLength_L
    0x00,	//wTotalLength_H

    //MIDI OUT Jack Descriptor (Embedded)
    0x09,	//bLength
    0x24,	//bDescriptorType
    0x03,	//bDescriptorSubType
    0x01,	//bJackType
    0x01,	//bJackID
    0x01,	//bNrInputPins
    0x02,	//BaSourceID(1)
    0x01,	//BaSourcePin(1)
    0x00,	//iJack

    //MIDI OUT Jack Descriptor (External)
    0x09,	//bLength
    0x24,	//bDescriptorType
    0x03,	//bDescriptorSubType
    0x02,	//bJackType
    0x02,	//bJackID
    0x01,	//bNrInputPins
    0x01,	//BaSourceID(1)
    0x01,	//BaSourcePin(1)
    0x00,	//iJack

    //���������� �������� ����� 1 BULK IN
    0x09,	//bLength
    0x05,	//bDescriptorType
    0x81,	//bEndpointAddress
    0x02,	//bmAttributes
    0x40,	//wMaxPacketSize_L
    0x00,	//wMaxPacketSize_H
    0x00,       //bInterval
    0x00,       //bRefresh
    0x00,       //bSynchAddress
    //���������� �������� ����� 1 IN
    0x05,	//bLength
    0x25,	//bDescriptorType
    0x01,	//bDescriptorSubType
    0x01,	//bNumEmbMIDIJack
    0x01,	//BaAssocJackID(1)

//���������� ����������
    0x09,	//bLength
    0x04,	//bDescriptorType
    0x02,	//b InterfaceNumber
    0x00,	//bAlternateSetting
    0x02,	//bNumEndpoints
    0x03,	//bInterfaceClass(0x03 - HID, 0xFF - custom)
    0x00,	//bInterfaceSubClass
    0x00,	//bInterfaceProtocol
    0x00,	//iInterface
    //HID ����������
    0x09,
    0x21,
    0x01,
    0x01,
    0x00,
    0x01,
    0x22,	//��� ������
    HID_REPORT_DESCRIPTOR_SIZE,   //������ �������
    0x00,
    //���������� �������� ����� 1 IN
    0x07,	//bLength
    0x05,	//bDescriptorType
    0x82,	//bEndpointAddress
    0x03,	//bmAttributes
    0x40,	//wMaxPacketSize_L
    0x00,	//wMaxPacketSize_H
    0x10,         //bInterval
    //���������� �������� ����� 1 OUT
    0x07,	//bLength
    0x05,	//bDescriptorType
    0x02,	//bEndpointAddress
    0x03,	//bmAttributes
    0x40,	//wMaxPacketSize_L
    0x00,	//wMaxPacketSize_H
    0x10         //bInterval
};

const uint8_t HID_ReportDescriptor[HID_REPORT_DESCRIPTOR_SIZE] =
{
	0x05, 0x01,                    // USAGE_PAGE (Generic Desktop)
	0x09, 0x00,                    // USAGE (Undefined)
	0xa1, 0x01,                    // COLLECTION (Application)

	0x85, 0x01,                    //   REPORT_ID (1)
	0x09, 0x00,                    //   USAGE (Undefined)
	0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
	0x26, 0xff, 0x00,              //   LOGICAL_MAXIMUM (255)
	0x75, 0x08,                    //   REPORT_SIZE (8)
	0x95, 0x0B,                    //   REPORT_COUNT (11)
	0x91, 0x82,                    //   OUTPUT (Data,Var,Abs,Vol)

        0x85, 0x02,                    //   REPORT_ID (2)
	0x09, 0x00,                    //   USAGE (Undefined)
	0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
	0x26, 0xff, 0x00,              //   LOGICAL_MAXIMUM (255)
	0x75, 0x08,                    //   REPORT_SIZE (8)
	0x95, 0x02,                    //   REPORT_COUNT (2)
	0x91, 0x82,                    //   OUTPUT (Data,Var,Abs,Vol)

	0x85, 0x03,                    //   REPORT_ID (3)
	0x09, 0x00,                    //   USAGE (Undefined)
	0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
	0x26, 0xff, 0x00,              //   LOGICAL_MAXIMUM (255)
	0x75, 0x08,                    //   REPORT_SIZE (8)
	0x95, 0x0B,                    //   REPORT_COUNT (11)
	0x81, 0x82,                    //   INPUT (Data,Var,Abs,Vol)
       
        0xc0                           // END_COLLECTION
};

const uint8_t USB_StringLangDescriptor[] = {
    0x04,	//bLength
    0x03,	//bDescriptorType
    0x09,	//wLANGID_L
    0x04	//wLANGID_H
};

const uint8_t USB_StringManufacturingDescriptor[] = {
    STRING_MANUFACTURING_DESCRIPTOR_SIZE_BYTE,		//bLength
    0x03,											//bDescriptorType
    'S', 0x00,										//bString...
    'O', 0x00,
    'B', 0x00,
    'S', 0x00
};

const uint8_t USB_StringProdDescriptor[] = {
    STRING_PRODUCT_DESCRIPTOR_SIZE_BYTE,		//bLength
    0x03,										//bDescriptorType
    'U', 0x00,									//bString...
    'S', 0x00,
    'B', 0x00,
    ' ', 0x00,
    'M', 0x00,
    'I', 0x00,
    'D', 0x00,
    'I', 0x00
};

const uint8_t USB_StringSerialDescriptor[STRING_SERIAL_DESCRIPTOR_SIZE_BYTE] =
{
    STRING_SERIAL_DESCRIPTOR_SIZE_BYTE,           /* bLength */
    0x03,        /* bDescriptorType */
    'R', 0,
    'H', 0,
    '-', 0,
    '0', 0,
    '0', 0,
    '0', 0,
    '1', 0
};
