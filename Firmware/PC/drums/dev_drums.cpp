#include "dev_drums.h"

dev_drums::dev_drums()
{
    USB_Device = NULL;
    dev = NULL;
    libusb_init(&ctx);
}

int dev_drums :: find(uint16_t VID, uint16_t PID, uint8_t interfase_n){
    libusb_device **devs;
    libusb_device_descriptor descr_dev;
    libusb_config_descriptor *descr_cfg;
    const libusb_interface *inter;
    const libusb_interface_descriptor *descr_inter;
    ssize_t cnt;

    cnt = libusb_get_device_list(ctx, &devs);
    for (int i = 0; i < cnt; i++){
        libusb_get_device_descriptor(devs[i], &descr_dev);
        if ((descr_dev.idVendor == VID) && (descr_dev.idProduct == PID)){
            libusb_get_config_descriptor(devs[i], 0, &descr_cfg);
            for (int j = 0; j < (int)descr_cfg -> bNumInterfaces; j++){
                inter = &descr_cfg -> interface[j];
                descr_inter = &inter -> altsetting[0];
                if (descr_inter->bInterfaceNumber == interfase_n){
                    dev = devs[i];
                    interfase = interfase_n;
                    return 0;
                }
            }
        }
    }
    return 1;
}

int dev_drums :: connect(uint8_t ep){
    if ((dev != NULL) && (USB_Device == NULL)){
        endpoint = ep;
        return libusb_open(dev, &USB_Device);
    }
    if (USB_Device != NULL) return 1; //already_connect
    return 2;                         //no_found
}

void dev_drums :: disconnect(){
    libusb_close(USB_Device);
    USB_Device = NULL;
}

int dev_drums :: drums_send(uint8_t *data, uint8_t len){
    int ret;
    int stat = libusb_claim_interface(USB_Device, interfase);
    if (stat == 0){
        stat = libusb_interrupt_transfer(USB_Device, endpoint, data, len, &ret, 100);
        libusb_release_interface(USB_Device, interfase);
        return stat;
    }
    return 1;
}

int dev_drums :: drums_save(){
    int ret;
    int stat = libusb_claim_interface(USB_Device, interfase);
    uint8_t cmd[] = {2, 1, 0};

    if (stat == 0){
        stat = libusb_interrupt_transfer(USB_Device, endpoint, cmd, 3, &ret, 100);
        libusb_release_interface(USB_Device, interfase);
        return stat;
    }
    return 1;
}

int dev_drums :: drums_change_midi_channel(uint8_t channel){
    int ret;
    int stat = libusb_claim_interface(USB_Device, interfase);
    uint8_t cmd[] = {2, 2, channel};

    if (stat == 0){
        stat = libusb_interrupt_transfer(USB_Device, endpoint, cmd, 3, &ret, 100);
        libusb_release_interface(USB_Device, interfase);
        return stat;
    }
    return 1;
}

int dev_drums :: drums_read(uint8_t channel, _settings *settings){
    int ret;
    int stat = libusb_claim_interface(USB_Device, interfase);
    uint8_t cmd[] = {2, 0, channel};
    uint8_t bytes[12];

    if (channel >= N_CHANNEL) return 2;

    if (stat == 0){
        stat = libusb_interrupt_transfer(USB_Device, endpoint, cmd, 3, &ret, 100);
        if (stat == 0){
            stat = libusb_interrupt_transfer(USB_Device, endpoint | 0x80, bytes, 12, &ret, 100);
            ByteArrayN12ToSettings(bytes, settings);
        }
        libusb_release_interface(USB_Device, interfase);
        return stat;
    }
    return 1;
}

int dev_drums :: drums_get_midi_channel(uint8_t *channel){
    int ret;
    int stat = libusb_claim_interface(USB_Device, interfase);
    uint8_t cmd[] = {2, 0, N_CHANNEL};
    uint8_t bytes[12];

    if (stat == 0){
        stat = libusb_interrupt_transfer(USB_Device, endpoint, cmd, 3, &ret, 100);
        if (stat == 0){
            stat = libusb_interrupt_transfer(USB_Device, endpoint | 0x80, bytes, 12, &ret, 100);
        }
        libusb_release_interface(USB_Device, interfase);
        *channel = bytes[2];
        return stat;
    }
    return 1;
}

void dev_drums :: SettingsToByteArrayN12(_settings s, uint8_t *bytes){
    bytes[0] = s.report;
    bytes[1] = s.channel;
    bytes[2] = (s.volume >> 8) & 0xFF;
    bytes[3] = s.volume & 0xFF;
    bytes[4] = (s.pitch >> 8) & 0xFF;
    bytes[5] = s.pitch & 0xFF;
    bytes[6] = (s.level_l >> 8) & 0xFF;
    bytes[7] = s.level_l & 0xFF;
    bytes[8] = (s.level_h >> 8) & 0xFF;
    bytes[9] = s.level_h & 0xFF;
    bytes[10] = (s.pause >> 8) & 0xFF;
    bytes[11] = s.pause & 0xFF;
}

void dev_drums :: ByteArrayN12ToSettings(uint8_t *bytes, _settings *s){
    s -> report = bytes[0];
    s -> channel = bytes[1];
    s -> volume = (uint16_t)(bytes[2] << 8) | bytes[3];
    s -> pitch = (uint16_t)(bytes[4] << 8) | bytes[5];
    s -> level_l = (uint16_t)(bytes[6] << 8) | bytes[7];
    s -> level_h = (uint16_t)(bytes[8] << 8) | bytes[9];
    s -> pause = (uint16_t)(bytes[10] << 8) | bytes[11];
}
