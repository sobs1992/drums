#ifndef DEV_DRUMS_H
#define DEV_DRUMS_H

#include "libusb.h"

typedef struct {
    uint8_t report;
    uint8_t channel;
    uint16_t volume;
    uint16_t pitch;
    uint16_t level_l;
    uint16_t level_h;
    uint16_t pause;
} _settings;

class dev_drums
{
public:
    _settings settings;
    static const int N_CHANNEL = 40;
    dev_drums();
    int find(uint16_t VID, uint16_t PID, uint8_t interfase_n);
    int connect(uint8_t ep);
    void disconnect();
    int drums_send(uint8_t *data, uint8_t len);
    int drums_save();
    int drums_read(uint8_t channel, _settings *settings);
    int drums_change_midi_channel(uint8_t channel);

    int drums_get_midi_channel(uint8_t *channel);

    void ByteArrayN12ToSettings(uint8_t *bytes, _settings *s);
    void SettingsToByteArrayN12(_settings s, uint8_t *bytes);
private:
    libusb_device* dev;
    libusb_context *ctx;
    libusb_device_handle *USB_Device;
    uint8_t endpoint;
    uint8_t interfase;
};

#endif // DEV_DRUMS_H
