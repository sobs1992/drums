#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QLabel *number[dev_drums :: N_CHANNEL];
    QLabel *col[5];
    //ScrollArea - виджет верхнего уровня, полосы прокрутки
    QScrollArea *scroll = new QScrollArea;
    //Window - обсласть прорисовки
    window = new QWidget;
    //Табличная разметка
    QGridLayout *grid = new QGridLayout;
    //Добавляем в ScrollArea виджет нашего окна
    scroll -> setWidget(window);
    //Натягиваем на окно нашу таблиную разметку
    window -> setLayout(grid);
    //Устанавливаем центральным виджетом ScrollArea
    setCentralWidget(scroll);
    //Устанавиваем фиксированные размеры окна
    this -> setFixedSize(QSize(630, 600));
    window -> setFixedSize(QSize(600, 1280));
    this -> resize(630, 600);

    this -> setWindowTitle("Drums configurator");
    QStatusBar *statusbar = statusBar();
    status = new QLabel();
    status -> setMinimumWidth(this -> width() - 200);
    statusbar -> addWidget(status);
    status -> setText("Drums disconnected");
    progress = new QProgressBar(this);
    statusbar -> addWidget(progress);
    progress -> setMaximum(dev_drums :: N_CHANNEL - 1);
    progress -> setVisible(false);

    //Маппер сигналов нажатий кнопок на 1 слот
    QSignalMapper *psigMapper_Send = new QSignalMapper(this);
    connect(psigMapper_Send, SIGNAL(mapped(const int)), this, SLOT(send_btns_clicked(const int)));

    QLabel *label_channel = new QLabel();
    label_channel -> setText("<span style=\" font-size:10pt; font-weight:600; color:#000000;\">MIDI канал: </span>");
    grid -> addWidget(label_channel, 0, 0, 1, 2);
    midi_channel = new QComboBox();
    for (int i = 1; i <= 16; i++){
        midi_channel -> addItem(QString :: number(i));
    }
    grid -> addWidget(midi_channel, 0, 2);
    connect(midi_channel, SIGNAL(currentIndexChanged(int)), this, SLOT(midi_channel_changed()));

    QLabel *label_settings = new QLabel();
    label_settings -> setAlignment(Qt :: AlignCenter);
    label_settings -> setText("<span style=\" font-size:10pt; font-weight:600; color:#000000;\">Настройки </span>");
    grid -> addWidget(label_settings, 0, 3);

    //Создаем, размещаем и соединяем со слотом кнопку обновить
    update_btn = new QPushButton("Прочитать");
    grid -> addWidget(update_btn, 0, 4);
    connect(update_btn, SIGNAL(clicked()), this, SLOT(update_btn_clicked()));

    save_btn = new QPushButton("Сохранить");
    grid -> addWidget(save_btn, 0, 5);
    connect(save_btn, SIGNAL(clicked()), this, SLOT(save_btn_clicked()));
    grid -> setVerticalSpacing(10);

    //Рисуем подписи столбцов
    for (int i = 0; i < 5; i++){
        col[i] = new QLabel;
        col[i] -> setAlignment(Qt :: AlignCenter);
        col[i] -> setWordWrap(true);
        grid -> addWidget(col[i], 1, i + 1);
        grid -> setColumnStretch(i + 1, 1);
    }
    col[0] -> setText("<span style=\" font-size:9pt; font-weight:600; color:#000000;\">Максимальный уровень </span>");
    col[1] -> setText("<span style=\" font-size:9pt; font-weight:600; color:#000000;\">Нота </span>");
    col[2] -> setText("<span style=\" font-size:9pt; font-weight:600; color:#000000;\">Нижний\nпорог </span>");
    col[3] -> setText("<span style=\" font-size:9pt; font-weight:600; color:#000000;\">Верхний\nпорог </span>");
    col[4] -> setText("<span style=\" font-size:9pt; font-weight:600; color:#000000;\">Пауза </span>");

    //Рисуем строки
    for (int i = 0; i < dev_drums :: N_CHANNEL; i++){
        //1 столбец, номер канала
        number[i] = new QLabel(QString :: number(i+1));
        grid -> addWidget(number[i], i+2, 0);
        //2 столбец, громкость
        volume[i] = new QSpinBox;
        volume[i] -> setMaximum(4095);
        grid -> addWidget(volume[i], i+2, 1);
        //2 столбец, нота
        pitch[i] = new QSpinBox;
        pitch[i] -> setMaximum(128);
        grid -> addWidget(pitch[i], i+2, 2);
        //3 столбец, нижний порог
        level_l[i] = new QSpinBox;
        level_l[i] -> setMaximum(4095);
        grid -> addWidget(level_l[i], i+2, 3);
        //4 столбец, верхний порог
        level_h[i] = new QSpinBox;
        level_h[i] -> setMaximum(4095);
        grid -> addWidget(level_h[i], i+2, 4);
        //5 столбец, пауза
        pause[i] = new QSpinBox;
        pause[i] -> setMaximum(65535);
        grid -> addWidget(pause[i], i+2, 5);

        //Соединяем Spinbox'ы от кнопки с маппером
        connect(volume[i], SIGNAL(valueChanged(int)), psigMapper_Send, SLOT(map()));
        connect(pitch[i], SIGNAL(valueChanged(int)), psigMapper_Send, SLOT(map()));
        connect(level_l[i], SIGNAL(valueChanged(int)), psigMapper_Send, SLOT(map()));
        connect(level_h[i], SIGNAL(valueChanged(int)), psigMapper_Send, SLOT(map()));
        connect(pause[i], SIGNAL(valueChanged(int)), psigMapper_Send, SLOT(map()));
        psigMapper_Send -> setMapping(volume[i], i);
        psigMapper_Send -> setMapping(pitch[i], i);
        psigMapper_Send -> setMapping(level_l[i], i);
        psigMapper_Send -> setMapping(level_h[i], i);
        psigMapper_Send -> setMapping(pause[i], i);
    }
    window -> setEnabled(false);
    timer = new QTimer(this);
    timer -> setInterval(100);
    connect(timer, SIGNAL(timeout()), this, SLOT(timer_update()));
    timer -> start();
}

void MainWindow :: timer_update(){
    static int connect_ok = 0;
    int stat = drums.find(0x0483, 0x5711, 2);
    if ((stat == 0) && (connect_ok == 0)){
        stat = drums.connect(2);
        if (stat == 0){
            connect_ok = 1;
            status -> setText("Drums connected");
            update_btn_clicked();
            window -> setEnabled(true);
        }
    } else {
        if (stat){
            drums.disconnect();
            status -> setText("Drums disconnected");
            connect_ok = 0;
            window -> setEnabled(false);
        }
    }
}

void MainWindow :: send_btns_clicked(const int id){
    uint8_t bytes[12];
    status -> setText("Send message ");
    drums.settings.report = 1;
    drums.settings.channel = (uint8_t)id;
    drums.settings.volume = (uint16_t)volume[id] -> value();
    drums.settings.pitch = (uint16_t)pitch[id] -> value();
    drums.settings.level_l = (uint16_t)level_l[id] -> value();
    drums.settings.level_h = (uint16_t)level_h[id] -> value();
    drums.settings.pause = (uint16_t)pause[id] -> value();
    drums.SettingsToByteArrayN12(drums.settings, bytes);
    timer -> blockSignals(true);
    if (!drums.drums_send(bytes, 12)){
        status -> setText(status -> text() + "OK");
    } else status -> setText(status -> text() + "ERROR");
    timer -> blockSignals(false);
}

void MainWindow :: update_btn_clicked(){
    _settings settings;
    int state;
    uint8_t ch_midi;
    timer -> blockSignals(true);

    status -> setText("Reading MIDI Channel ");
    if (drums.drums_get_midi_channel(&ch_midi) == 0){
        midi_channel -> setCurrentIndex(ch_midi);
        status -> setText(status -> text() + " OK");
    } else {
        status -> setText(status -> text() + " Error");
        timer -> blockSignals(false);
        return;
    }
    progress -> setVisible(true);
    for (uint8_t i = 0; i < dev_drums :: N_CHANNEL; i++){
        status -> setText("Reading channel " + QString :: number(i+1));
        progress -> setValue(i);
        state = drums.drums_read(i, &settings);
        if (state == 0){
            status -> setText(status -> text() + " OK");
            volume[i] -> setValue(settings.volume);
            pitch[i] -> setValue(settings.pitch);
            level_l[i] -> setValue(settings.level_l);
            level_h[i] -> setValue(settings.level_h);
            pause[i] -> setValue(settings.pause);
        } else {
            status -> setText(status -> text() + " Error");
            break;
        }
    }
    progress -> setVisible(false);
    timer -> blockSignals(false);
}

void MainWindow :: save_btn_clicked(){
    drums.drums_save();
    status -> setText("Save settings");
}

void MainWindow :: midi_channel_changed(){
    drums.drums_change_midi_channel((uint8_t)midi_channel -> currentIndex());
    status -> setText("Change MIDI channel");
}

MainWindow::~MainWindow()
{
    delete ui;
}
