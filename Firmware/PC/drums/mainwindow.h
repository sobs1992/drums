#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>
#include <QGridLayout>
#include <QWidget>
#include <QScrollArea>
#include <QSignalMapper>
#include <dev_drums.h>
#include <QTimer>
#include <QProgressBar>
#include <QComboBox>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:

    Ui::MainWindow *ui;
    QWidget *window;
    QSpinBox *pitch[dev_drums :: N_CHANNEL];
    QSpinBox *level_l[dev_drums :: N_CHANNEL];
    QSpinBox *level_h[dev_drums :: N_CHANNEL];
    QSpinBox *pause[dev_drums :: N_CHANNEL];
    QSpinBox *volume[dev_drums :: N_CHANNEL];
    QPushButton *update_btn;
    QPushButton *save_btn;
    dev_drums drums;
    QTimer *timer;
    QProgressBar *progress;
    QLabel *status;
    QComboBox *midi_channel;

private slots:
    void send_btns_clicked(const int id);
    void update_btn_clicked();
    void save_btn_clicked();
    void midi_channel_changed();
    void timer_update();
};

#endif // MAINWINDOW_H
